﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HaveIBeenPawnedChecker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private List<string> GetSHA1HashOfAllPasswords(List<string> passwords)
        {
            List<string> hashes = new List<string>();

            SHA1 sha = new SHA1CryptoServiceProvider();

            foreach (string password in passwords)
            {
                byte[] passwordBytes = Encoding.ASCII.GetBytes(password);
                byte[] hashBytes = sha.ComputeHash(passwordBytes);

                string hash = ByteArrayToString(hashBytes).ToUpper();


                hashes.Add(hash);

            }

            return hashes;

        }

        private string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public List<string> InjectFakePasswords(List<string> passwords, int count)
        {
            Random r = new Random();

            for (int i = 0; i < count; i++)
            {
                int random = r.Next(0, 100000);

                string fakePassword = "fake" + random;


                passwords.Insert(r.Next(0, passwords.Count+1), fakePassword);
            }

            return passwords;
        }

        public List<string> HaveIBeenPawnedCheckAllPasswords(List<string> passwords)
        {
            List<string> results = new List<string>();

            passwords = InjectFakePasswords(passwords, 100);

            List<string> hashes = GetSHA1HashOfAllPasswords(passwords);

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            int i = 0;
            var client = new WebClient();
            foreach (string hash in hashes)
            {
                string firstHalf = hash.Substring(0, 5);
                string secondHalf = hash.Substring(5);
                var text = client.DownloadString("https://api.pwnedpasswords.com/range/" + firstHalf);


                int startIndex = text.IndexOf(secondHalf);

                if (startIndex >= 0)
                {
                    int endIndex = text.IndexOf("\r\n", startIndex);
                    if(endIndex >= 0)
                    {
                        results.Add(passwords[i] + "," + text.Substring(startIndex, endIndex-startIndex));
                    }
                    else
                    {
                        results.Add(passwords[i] + "," + text.Substring(startIndex));
                    }
                    
                }
                i++;
            }

            return results;



        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            string[] splitText = textBoxPasswords.Text.Split(new string[] { "\r\n" },StringSplitOptions.RemoveEmptyEntries);

            List<string> results = HaveIBeenPawnedCheckAllPasswords(splitText.ToList());


            textBoxResults.Text = String.Join("\r\n",results);

        }
    }
}
